package client;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ClientData implements AutoCloseable {

    private Socket socket;
    private Scanner scanner;
    private PrintWriter printWriter;
    private boolean isPlaying = false;

    public ClientData(ServerSocket ss) throws Exception {
        socket = ss.accept();
        scanner = new Scanner(socket.getInputStream());
        printWriter = new PrintWriter(socket.getOutputStream());
    }

    public void close() throws Exception {
        if (socket == null) return;
        socket.close();
    }

    Scanner getScanner() {
        return scanner;
    }

    PrintWriter getPrintWriter() {
        return printWriter;
    }
}
