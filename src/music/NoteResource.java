package music;

public class NoteResource {

    private int midiCode;
    private String name;

    public NoteResource(int midiCode, String name) {
        this.midiCode = midiCode;
        this.name = name;
    }

    public int getMidiCode() {
        return midiCode;
    }

    public String getName() {
        return name;
    }
}
