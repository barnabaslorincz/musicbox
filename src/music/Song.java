package music;


import java.util.ArrayList;

public class Song implements Cloneable{

    private int id;
    private String title;
    private ArrayList<Note> notes;

    public Song(int id, String title) {
        this.id = id;
        this.title = title;
        this.notes = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<Note> getNotes() {
        return notes;
    }

    public void addNote(Note note) {
        this.notes.add(note);
    }

    public void setNotes(ArrayList<Note> notes) {
        this.notes = notes;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


}
