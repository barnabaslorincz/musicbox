package music;

import client.ClientOperation;

import java.util.ArrayList;

public class Note {

    private String note; // C is the default note
    private String lyrics; // No lyrics added by default
    private int noteLength; // length of the note

    public Note(String note, String lyrics, int noteLength) {
        this.note = note;
        this.lyrics = lyrics;
        this.noteLength = noteLength;
    }

    public static class Builder {
        private String note = "C"; // C is the default note
        private String lyrics = "???"; // No lyrics added by default
        private int noteLength = 0; // length of the note

        public Builder(){}

        public Builder setNote(String note) {
            this.note = note;
            return this;
        }

        public Builder setLyrics(String lyrics) {
            this.lyrics = lyrics;
            return this;
        }

        public Builder setNoteLength(int noteLength) {
            this.noteLength = noteLength;
            return this;
        }

        public Note build() {
            return new Note(note, lyrics, noteLength);
        }
    }

    public String getNote() {
        return note;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public int getNoteLength() {
        return noteLength;
    }

    public static String replaceRepetitions(String notes) {
        String[] notesWithRepetitions = notes.split(ClientOperation.DELIMITER);
        ArrayList<String> notesWithoutRepetitions = new ArrayList<>();
        for (int i = 0; i < notesWithRepetitions.length; i+=2) {
            if (notesWithRepetitions[i].equals("REP")) {
                int noteCount = Integer.parseInt(notesWithRepetitions[i+1].split(ClientOperation.REPETITION_DELIMITER)[0]);
                int repetitionCount = Integer.parseInt(notesWithRepetitions[i+1].split(ClientOperation.REPETITION_DELIMITER)[1]);
                for (int j = 0; j < repetitionCount; j++) {
                    for (int k = noteCount*2-2; k >= 0; k-=2) {
                        notesWithoutRepetitions.add(notesWithRepetitions[i-2-k]);
                        notesWithoutRepetitions.add(notesWithRepetitions[i-1-k]);
                    }
                }
            }
            else {
                notesWithoutRepetitions.add(notesWithRepetitions[i]);
                notesWithoutRepetitions.add(notesWithRepetitions[i+1]);
            }
        }
        return String.join(ClientOperation.DELIMITER, notesWithoutRepetitions);
    }

    public static Note toResourceNote(String rawNote, String rawLength) {
        int length = Integer.parseInt(rawLength);
        String note = toResourceNoteString(rawNote);
        return new Note.Builder()
                .setNote(note)
                .setLyrics("???")
                .setNoteLength(length)
                .build();
    }

    public static String toResourceNoteString(String rawNote) {
        String note;
        String[] noteParts = rawNote.split(ClientOperation.NOTE_MODIFICATION_DELIMITER);
        StringBuilder noteStringBuilder = new StringBuilder();
        String noteBase = noteParts[0];
        note = noteBase.substring(0, 1);
        noteStringBuilder.append(note);
        if (noteBase.length() == 2) {
            noteStringBuilder.append(noteBase.substring(1));
        }
        if (noteParts.length == 2) {
            int octaveModificationToAppend = Integer.parseInt(noteParts[1]) + 4;
            noteStringBuilder.append(octaveModificationToAppend);
        }
        else {
            if (!note.equals("R")) {
                noteStringBuilder.append("4");
            }
        }
        return noteStringBuilder.toString();
    }

    public static String toPublicNote(String rawNote) {
        if (rawNote.equals("R")) {
            return rawNote;
        }
        StringBuilder publicNoteBuilder = new StringBuilder();
        publicNoteBuilder.append(rawNote.substring(0,1));
        if (rawNote.length() == 2) {
            int publicModification = Integer.parseInt(rawNote.substring(1,2)) - 4;
            if (publicModification != 0) {
                publicNoteBuilder.append(ClientOperation.NOTE_MODIFICATION_DELIMITER);
                publicNoteBuilder.append(publicModification);
            }
            return publicNoteBuilder.toString();
        }
        else if (rawNote.length() == 3) {
            publicNoteBuilder.append(rawNote.substring(1,2));
            int publicModification = Integer.parseInt(rawNote.substring(2,3)) - 4;
            if (publicModification != 0) {
                publicNoteBuilder.append(ClientOperation.NOTE_MODIFICATION_DELIMITER);
                publicNoteBuilder.append(publicModification);
            }
            return publicNoteBuilder.toString();
        }
        return "";
    }


    public void nextHalfNote() {
        if (this.note.equals("R")) {
            return;
        }
        this.note = NoteList.getNoteNameByMidiCode(NoteList.getMidiCodeByNoteName(this.note)+1);
    }

    public void previousHalfNote() {
        if (this.note.equals("R")) {
            return;
        }
        this.note = NoteList.getNoteNameByMidiCode(NoteList.getMidiCodeByNoteName(this.note)-1);
    }
}