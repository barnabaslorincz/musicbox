package music;

public class PlayListItem {

    private int id;
    private int tempoInMilliseconds;
    private Song song;
    private boolean isPlaying;

    public PlayListItem(int id, int tempoInMilliseconds, Song song, boolean isPlaying) {
        this.id = id;
        this.tempoInMilliseconds = tempoInMilliseconds;
        this.song = song;
        this.isPlaying = isPlaying;
    }

    public int getId() {
        return id;
    }

    public int getTempoInMilliseconds() {
        return tempoInMilliseconds;
    }

    public void setTempoInMilliseconds(int tempoInMilliseconds) {
        this.tempoInMilliseconds = tempoInMilliseconds;
    }

    public Song getSong() {
        return song;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }
}
