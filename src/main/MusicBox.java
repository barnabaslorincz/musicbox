package main;

import client.ClientData;
import client.ClientThread;
import music.PlayListItem;
import music.Song;

import java.util.*;
import java.net.*;

public class MusicBox {

    private static final Set<ClientData> otherClients = new HashSet<>();
    private static final ArrayList<Song> songs = new ArrayList<>();
    private static final ArrayList<PlayListItem> playList = new ArrayList<>();

    public static void main(String[] args) throws Exception {

        try (
                ServerSocket ss = new ServerSocket(40000);
        ) {
            while (true) {
                ClientData client = new ClientData(ss);
                synchronized (otherClients) {
                    otherClients.add(client);
                }
                new Thread(new ClientThread(client)).start();
            }
        }
    }

    public static Set<ClientData> getOtherClients() {
        return otherClients;
    }

    public static ArrayList<Song> getSongs() {
        return songs;
    }

    public static ArrayList<PlayListItem> getPlayList() {
        return playList;
    }
}

