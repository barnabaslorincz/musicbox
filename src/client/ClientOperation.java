package client;

public class ClientOperation {

    public static final String DELIMITER = " ";
    public static final String REPETITION_DELIMITER = ";";
    public static final String NOTE_MODIFICATION_DELIMITER = "/";

    public static final String ADD = "add";
    public static final String ADD_LYRICS = "addlyrics";
    public static final String PLAY = "play";
    public static final String CHANGE = "change";
    public static final String STOP = "stop";

}
