package client;

import main.MusicBox;
import music.Note;
import music.PlayListItem;
import music.Song;

public class ClientThread implements Runnable {

    private ClientData client;

    public ClientThread(ClientData client) {
        this.client = client;
    }

    @Override
    public void run() {
        while (client.getScanner().hasNextLine()) {
            String clientInput = client.getScanner().nextLine();
            String clientOperation = clientInput.split(ClientOperation.DELIMITER)[0];
            switch (clientOperation) {
                case ClientOperation.ADD:
                    add(clientInput, client);
                    break;
                case ClientOperation.ADD_LYRICS:
                    addLyrics(clientInput, client);
                    break;
                case ClientOperation.PLAY:
                    try {
                        play(clientInput, client);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case ClientOperation.CHANGE:
                    change(clientInput);
                    break;
                case ClientOperation.STOP:
                    stop(clientInput);
                    break;
            }
        }

        synchronized (MusicBox.getOtherClients()) {
            MusicBox.getOtherClients().remove(client);
            try {
                client.close();
            } catch (Exception e) {
                // won't happen
            }
        }
    }

    private static void add(String clientInput, ClientData client) {
        String songTitle = clientInput.split(ClientOperation.DELIMITER)[1];
        Song song = new Song(MusicBox.getSongs().size(), songTitle);
        if (client.getScanner().hasNextLine()) {
            synchronized (MusicBox.getSongs()) {
                String noteInput = client.getScanner().nextLine();
                String normalizedInput = Note.replaceRepetitions(noteInput);
                String[] notes = normalizedInput.split(ClientOperation.DELIMITER);
                for (int i = 0; i < notes.length; i+=2) {
                    Note note = Note.toResourceNote(notes[i], notes[i+1]);
                    song.addNote(note);
                }
                Song foundSong = null;
                for (Song s : MusicBox.getSongs()) {
                    if (s.getTitle().equals(songTitle)) {
                        foundSong = s;
                    }
                }
                if (foundSong == null) {
                    MusicBox.getSongs().add(song);
                }
                else {
                    MusicBox.getSongs().set(MusicBox.getSongs().indexOf(foundSong), song);
                }
            }
        }
    }

    private static void addLyrics(String clientInput, ClientData client) {
        String songTitle = clientInput.split(ClientOperation.DELIMITER)[1];
        Song song = null;
        for (Song s : MusicBox.getSongs()) {
            if (s.getTitle().equals(songTitle)) {
                song = s;
            }
        }
        if (song != null) {
            int songIndex = MusicBox.getSongs().indexOf(song);
            if (client.getScanner().hasNextLine()) {
                synchronized (MusicBox.getSongs()) {
                    String lyricsInput = client.getScanner().nextLine();
                    String[] lyrics = lyricsInput.split(ClientOperation.DELIMITER);
                    int pauseCount = 0;
                    for (int i = 0; i < lyrics.length; i++) {
                        if (MusicBox.getSongs().get(songIndex).getNotes().get(i+pauseCount).getNote().equals("R")) {
                            MusicBox.getSongs().get(songIndex).getNotes().get(i+pauseCount).setLyrics("");
                            pauseCount++;
                        }
                        else {
                            MusicBox.getSongs().get(songIndex).getNotes().get(i+pauseCount).setLyrics(lyrics[i]);
                        }
                    }
                }
            }
        }
    }

    private static void change(String clientInput) {
        String[] changeCommands = clientInput.split(ClientOperation.DELIMITER);
        int playIndex = Integer.parseInt(changeCommands[1])-1;
        int tempoInMilliseconds = Integer.parseInt(changeCommands[2]);
        Song song = null;
        synchronized (MusicBox.getSongs()) {
            for (Song s : MusicBox.getSongs()) {
                if (s.getTitle().equals(MusicBox.getPlayList().get(playIndex + 1).getSong().getTitle())) {
                    try {
                        song = (Song) s.clone();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        synchronized (MusicBox.getPlayList()) {
            if (MusicBox.getPlayList().get(playIndex+1) != null) {
                MusicBox.getPlayList().get(playIndex+1).setTempoInMilliseconds(tempoInMilliseconds);
                if (changeCommands.length == 4 && song != null) {
                    int transponateSong = Integer.parseInt(changeCommands[3]);
                    if (transponateSong > 0) {
                        for (int i = 0; i < transponateSong; i++) {
                            for (Note n : song.getNotes()) {
                                n.nextHalfNote();
                            }
                        }
                        MusicBox.getPlayList().get(playIndex+1).getSong().setNotes(song.getNotes());
                    } else if (transponateSong < 0) {
                        for (int i = 0; i < Math.abs(transponateSong); i++) {
                            for (Note n : song.getNotes()) {
                                n.previousHalfNote();
                            }
                        }
                        MusicBox.getPlayList().get(playIndex+1).getSong().setNotes(song.getNotes());
                    }
                }
            }
        }
    }

    private static void play(String clientInput, ClientData client) throws InterruptedException {
        String[] playCommands = clientInput.split(ClientOperation.DELIMITER);
        String songTitle = playCommands[3];
        int tempoInMilliseconds = Integer.parseInt(playCommands[1]);
        int transponateSong = Integer.parseInt(playCommands[2]);
        int id;
        Song song = null;
        synchronized (MusicBox.getSongs()) {
            for (Song s : MusicBox.getSongs()) {
                if (s.getTitle().equals(songTitle)) {
                    try {
                        song = (Song) s.clone();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (song != null) {
            synchronized (MusicBox.getPlayList()) {
                id = MusicBox.getPlayList().size()+1;
                if (transponateSong > 0) {
                    for (int i = 0; i < transponateSong; i++) {
                        for (Note n : song.getNotes()) {
                            n.nextHalfNote();
                        }
                    }
                } else if (transponateSong < 0) {
                    for (int i = 0; i < Math.abs(transponateSong); i++) {
                        for (Note n : song.getNotes()) {
                            n.previousHalfNote();
                        }
                    }
                }
                PlayListItem playListItem = new PlayListItem(id, tempoInMilliseconds, song, true);
                MusicBox.getPlayList().add(playListItem);
            }
        }
        else {
            return;
        }
        String note;
        String lyrics;
        int length;
        int tempo;
        client.getPrintWriter().println("Playing " + id);
        for (int i = 0; i < MusicBox.getPlayList().get(id-1).getSong().getNotes().size(); i++) {
            if (MusicBox.getPlayList().get(id-1).isPlaying()) {
                synchronized (MusicBox.getPlayList()) {
                    Note n = MusicBox.getPlayList().get(id-1).getSong().getNotes().get(i);
                    note = Note.toPublicNote(n.getNote());
                    lyrics = n.getLyrics();
                    length = n.getNoteLength();
                    tempo  = MusicBox.getPlayList().get(id-1).getTempoInMilliseconds();
                }
                client.getPrintWriter().println(note + ' ' + lyrics);
                client.getPrintWriter().flush();
                Thread.sleep(length*tempo);
            }
            else {
                break;
            }
        }
        client.getPrintWriter().println("FIN");
        client.getPrintWriter().flush();
    }

    private static void stop(String clientInput) {
        String[] stopCommands = clientInput.split(ClientOperation.DELIMITER);
        int songId = Integer.parseInt(stopCommands[1]);
        synchronized (MusicBox.getPlayList()) {
            for (PlayListItem playListItem: MusicBox.getPlayList()) {
                if (playListItem.getId() == songId) {
                    playListItem.setPlaying(false);
                }
            }
        }
    }



}
