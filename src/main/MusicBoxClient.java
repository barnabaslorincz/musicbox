package main;

import client.ClientOperation;
import music.Note;
import music.NoteList;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class MusicBoxClient {

    private static MidiChannel midiChannel;

    public static void main(String[] args) throws Exception {
        String MACHINE = "localhost";
        int PORT = 40000;
        try (
                Socket s = new Socket(MACHINE, PORT);
                Scanner sc = new Scanner(s.getInputStream());
                Scanner sc2 = new Scanner(System.in);
                PrintWriter pw = new PrintWriter(s.getOutputStream())) {

            System.out.println("Connected to server.");
            Synthesizer synth = MidiSystem.getSynthesizer();
            synth.open();
            final MidiChannel[] mc = synth.getChannels();
            Instrument[] instr = synth.getDefaultSoundbank().getInstruments();
            synth.loadInstrument(instr[90]);
            midiChannel = mc[5];
                while (sc2.hasNextLine()) {
                    String inputLine = sc2.nextLine();
                    pw.println(inputLine);
                    pw.flush();
                    while (sc.hasNextLine()) {
                        String nextLine = sc.nextLine();
                        System.out.println(nextLine);
                        midiChannel.allNotesOff();
                        if (nextLine.equals("FIN")) {
                            break;
                        }
                        else if (!nextLine.equals("R")) {
                            playSound(nextLine);
                        }
                    }
                }
        }
    }

    private static void playSound(String nextLine) {
        String rawNote = nextLine.split(ClientOperation.DELIMITER)[0];
        String note = Note.toResourceNoteString(rawNote);
        int midiCode = NoteList.getMidiCodeByNoteName(note);
        midiChannel.noteOn(midiCode, 100);
    }
}

